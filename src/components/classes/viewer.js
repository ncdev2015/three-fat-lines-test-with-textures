import * as THREE from 'three'
//import { Wireframe } from 'three/examples/jsm/lines/Wireframe'
import { WireframeGeometry2 } from 'three/examples/jsm/lines/WireframeGeometry2';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import Stats from 'three/examples/jsm/libs/stats.module'
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';

export default class Viewer {
    constructor(width, height) {
        // Width and height
        this.width = width;
        this.height = height;        
        
        // Setup renderer
        this.renderer = new THREE.WebGLRenderer({antialias: true});
        this.renderer.setPixelRatio(window.devicePixelRatio)
        this.renderer.setClearColor(0x000000, 0.0)
        this.renderer.setSize(this.width, this.height);

        // Setup camera
        this.camera = new THREE.PerspectiveCamera(75, this.width / this.height, 0.1, 1000);
        this.camera.position.set(2,2,2);

        // Setup controls
        this.orbitControls = new OrbitControls(this.camera, this.renderer.domElement);

        // Setup grid
        this.grid = new THREE.GridHelper(1000, 1000);

        // Setup stats
        this.stats = new Stats();

        // Setup scene
        this.scene = new THREE.Scene();
        this.scene.add(this.grid);

        this.loadObjectWithTexture();

        // Método 1
        //this.drawCubeWithMultiplesCubes(1,1,1, 30);

        // Método 2
        //this.drawCubeMethod2();

        // Método 3
        // Recibe:
        // + Punto inicial
        // + Ancho, Largo y Profundidad
        // + Tamaño de ancho línea
        this.drawCubeWithFatLines(new THREE.Vector3(0,0,0), new THREE.Vector3(1,1,1), 0.005);

        // Setup Event Listeners
        window.addEventListener('resize', () => {
            this.onWindowResize();
        });

        // Main loop
        this.animate();
    }

    onWindowResize() {
        this.width = window.innerWidth;
        this.height = window.innerHeight;

        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    }

    animate() {
        const render = () => {
            requestAnimationFrame(render);

            this.orbitControls.update();
            this.renderer.render(this.scene, this.camera);
            this.stats.update();
        }
        render();        
    }

    loadObjectWithTexture() {
        const texture = new THREE.TextureLoader().load( 'resources/images/wood.jpg' );

        const geometry = new THREE.BoxGeometry( 1, 1, 1 );

        const material = new THREE.MeshBasicMaterial( { map: texture } );

        const mesh = new THREE.Mesh( geometry, material );

        mesh.position.set(0.5,0.5,0.5)

        this.scene.add(mesh);
    }

    drawLinesWidth(w = 0, h = 0, d = 0, xPosInit, yPosInit, zPosInit) {
        // Others
        let geo = new THREE.PlaneGeometry(w,h);       
        const geometry = new WireframeGeometry2(geo);

        const material = new LineMaterial({
            color: 0x00ff00,
            linewidth: 0.005,   
            wireframe: false
        })

        console.log(d);

        const cube = new THREE.Mesh( geometry, material );

        cube.position.set(xPosInit + w/2, yPosInit + h/2,zPosInit);

        this.scene.add( cube );
    }

    drawCubeMethod2() {
        let geo = new THREE.BoxGeometry(1,1);       
        const geometry = new WireframeGeometry2(geo);

        //const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        const material = new LineMaterial({
            color: 0x00ff00,
            linewidth: 0.005,   
            wireframe: false
        })

        const cube = new THREE.Mesh( geometry, material );

        cube.position.set(0.5,0.5,0.5);

        this.scene.add( cube );
    }

    drawSimpleCube(x, y, z) {
        const geometry = new THREE.BoxGeometry( x, y, z );
        const edges = new THREE.EdgesGeometry( geometry );
        const line = new THREE.LineSegments( edges, new THREE.LineBasicMaterial( { color: 0xff0000 } ) );

        line.position.set(0.5, 0.5, 0.5);

        this.scene.add( line );
    }

    drawCubeWithMultiplesCubes(x, y, z, lineWidth) {
        let increment = 0.001;
        for (let i = 0; i < lineWidth; i++) {
            this.drawSimpleCube(x + increment, y + increment, z + increment);
            increment += 0.001;
        }
    }

    drawFatLine( p0, direction, large, newLineWidth = 1 ) {
        
        const geo = new THREE.PlaneGeometry(large, 0);
        const geometry = new WireframeGeometry2(geo);
        
        const material = new LineMaterial({
            color: 0x00ff00,
            linewidth: newLineWidth
        })

        const line = new THREE.Mesh( geometry, material );        

        if (direction.x == 0 && direction.y == 1 && direction.z == 0) {
            line.rotation.z = Math.PI / 2;
            line.position.set(p0.x, p0.y + large/2, p0.z);
        }
        else if (direction.x == 0 && direction.y == 0 && direction.z == 1) {
            line.rotation.y = Math.PI / 2;
            line.position.set(p0.x, p0.y, p0.z + large/2);
        }
        else if (direction.x == -1 && direction.y == 0 && direction.z == 0) {
            line.position.set(p0.x - large/2, p0.y, p0.z);
        }
        else if (direction.x == 0 && direction.y == -1 && direction.z == 0) {
            line.rotation.z = Math.PI / 2;
            line.position.set(p0.x, p0.y - large/2, p0.z);
        }
        else if (direction.x == 0 && direction.y == 0 && direction.z == -1) {
            line.rotation.y = Math.PI / 2;
            line.position.set(p0.x, p0.y, p0.z - large/2);
        }
        else {
            line.position.set(p0.x + large / 2, p0.y, p0.z);
        }

        this.scene.add( line );
    }

    drawCubeWithFatLines(p0, size, newLineWidth) {
        this.drawFatLine( p0, new THREE.Vector3(1,0,0), size.x, newLineWidth )
        this.drawFatLine( p0, new THREE.Vector3(0,1,0), size.y, newLineWidth )
        this.drawFatLine( p0, new THREE.Vector3(0,0,1), size.z, newLineWidth )
        
        this.drawFatLine( new THREE.Vector3( p0.x + size.x, p0.y + size.y, p0.z + size.z ), new THREE.Vector3(-1,0,0), size.x, newLineWidth )
        this.drawFatLine( new THREE.Vector3( p0.x + size.x, p0.y + size.y, p0.z + size.z ), new THREE.Vector3(0,-1,0) , size.y, newLineWidth )
        this.drawFatLine( new THREE.Vector3( p0.x + size.x, p0.y + size.y, p0.z + size.z ), new THREE.Vector3(0,0,-1) , size.z, newLineWidth )

        this.drawFatLine( new THREE.Vector3( p0.x + size.x, p0.y, p0.z ), new THREE.Vector3(0,1,0), size.y, newLineWidth )
        this.drawFatLine( new THREE.Vector3( p0.x + size.x, p0.y, p0.z ), new THREE.Vector3(0,0,1), size.z, newLineWidth )
        
        this.drawFatLine( new THREE.Vector3( p0.x, p0.y + size.y, p0.z ), new THREE.Vector3(1,0,0), size.x, newLineWidth )
        this.drawFatLine( new THREE.Vector3( p0.x, p0.y + size.y, p0.z ), new THREE.Vector3(0,0,1), size.z, newLineWidth )

        this.drawFatLine( new THREE.Vector3( p0.x, p0.y, p0.z + size.z), new THREE.Vector3(1,0,0), size.x, newLineWidth )
        this.drawFatLine( new THREE.Vector3( p0.x, p0.y, p0.z + size.z), new THREE.Vector3(0,1,0), size.y, newLineWidth )
    }

}